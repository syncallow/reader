@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if(session('status'))
                                <div class="p-2">
                                    <div class="alert alert-success alert-dismissible fade show">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                        </button> {{ session('status') }}</div>
                                </div>
                            @endif
                                <h4 class="card-title">Roles</h4>
                                <a href="{{ route('admin.roles.create') }}" class="btn mb-1 btn-outline-primary">Create Role</a>
                            <div class="active-member">
                                <div class="table-responsive">
                                    <table class="table table-xs mb-0">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($roles as $role)
                                        <tr>
                                            <td>{{ $role->title }}</td>
                                            <td>
                                                <span><a href="{{ route('admin.roles.edit', $role->id) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a><a href="#" onclick="event.preventDefault(); document.getElementById('delRole').submit(); " data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-close color-danger"></i></a></span>
                                                <form id="delRole" action="{{ route('admin.roles.delete', $role->id) }}" class="d-none" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="pt-2">
                                        {{ $roles->links('vendor.pagination.bootstrap-4') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
