@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit {{ $user->name }} User</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.users.update', $user->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Email <span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}" readonly>
                                            @error('email')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control" placeholder="Your Name" value="{{ $user->name }}" required>
                                            @error('name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Set New Password</label>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                            @error('password')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>New Password Confirm</label>
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                                            @error('password_confirmation')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Profile Image</label>
                                        @if($user->profile_image)
                                            <div class="mb-3">
                                                <img src="{{ asset('/storage/' . $user->profile_image) }}" class="rounded-circle" width="100px" height="100px" alt="">
                                            </div>
                                            @endif
                                        <input type="file" class="form-control-file" name="profile_image">
                                        @error('profile_image')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label>Role <span class="text-danger">*</span></label>
                                            <select id="role" class="form-control" name="role_id" required>
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}" {{ $user->role_id == $role->id ? 'selected' : '' }}>{{ $role->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('role_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-10">
                                            <label>Job</label>
                                            <input type="text" class="form-control" name="job" value="{{ $user->job }}">
                                            @error('job')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>About me</label>
                                        <textarea class="form-control h-150px" rows="6" id="about_me" name="about_me">{{ $user->about_me }}</textarea>
                                        @error('about_me')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Facebook</label>
                                            <input type="text" name="facebook_link" class="form-control" placeholder="Your facebook" value="{{ $user->facebook_link }}">
                                            @error('facebook_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Twitter</label>
                                            <input type="text" name="twitter_link" class="form-control" placeholder="Your twitter" value="{{ $user->twitter_link }}">
                                            @error('twitter_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Github</label>
                                            <input type="text" name="github_link" class="form-control" placeholder="Your github" value="{{ $user->github_link }}">
                                            @error('github_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Web</label>
                                            <input type="text" name="web_link" class="form-control" placeholder="Your web" value="{{ $user->web_link }}">
                                            @error('web_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-dark">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
