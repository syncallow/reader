@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create User</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.users.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Email <span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                                            @error('email')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Name <span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control" placeholder="Your Name" required>
                                            @error('name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Password <span class="text-danger">*</span></label>
                                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                                            @error('password')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Password Confirm <span class="text-danger">*</span></label>
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                                            @error('password_confirmation')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Profile Image</label>
                                        <input type="file" class="form-control-file" name="profile_image">
                                        @error('profile_image')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label>Role <span class="text-danger">*</span></label>
                                            <select id="role" class="form-control" name="role_id" required>
                                                <option selected="selected">Choose...</option>
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}">{{ $role->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('role_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-10">
                                            <label>Job</label>
                                            <input type="text" class="form-control" name="job">
                                            @error('job')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>About me</label>
                                        <textarea class="form-control h-150px" rows="6" id="about_me" name="about_me"></textarea>
                                        @error('about_me')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Facebook</label>
                                            <input type="text" name="facebook_link" class="form-control" placeholder="Your facebook">
                                            @error('facebook_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Twitter</label>
                                            <input type="text" name="twitter_link" class="form-control" placeholder="Your twitter">
                                            @error('twitter_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Github</label>
                                            <input type="text" name="github_link" class="form-control" placeholder="Your github">
                                            @error('github_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Web</label>
                                            <input type="text" name="web_link" class="form-control" placeholder="Your web">
                                            @error('web_link')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-dark">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
