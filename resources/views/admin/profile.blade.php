@extends('admin.layouts.main')

@section('content')
    <div class="content-body">
        <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-sm-4 col-xl-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="media align-items-center mb-4">
                                    <img class="circle-rounded mr-3" src="{{ asset( $user->profile_image) }}" width="80" height="80" alt="">
                                    <div class="media-body">
                                        <h3 class="mb-0">{{ $user->name }}</h3>
                                        <p class="text-muted mb-0">{{ $user->job }}</p>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <div class="col">
                                        <div class="card card-profile text-center">
                                            <span class="mb-1 text-primary"><i class="fa fa-newspaper-o"></i></span>
                                            <h3 class="mb-0">{{ $user->posts->count() }}</h3>
                                            <p class="text-muted px-4">Posts</p>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="card card-profile text-center">
                                            <span class="mb-1 text-warning"><i class="icon-bubbles"></i></span>
                                            <h3 class="mb-0">{{ $user->comments->count() }}</h3>
                                            <p class="text-muted">Comments</p>
                                        </div>
                                    </div>
                                </div>

                                <h4>About Me</h4>
                                <p class="text-muted">{{ $user->about_me}}</p>
                                <ul class="card-profile__info">
                                    <li><strong class="text-dark mr-4">Email</strong> <span>{{ $user->email }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-xl-9">
                        <h4 class="d-inline ">Latest posts</h4>
                        <div class="row mt-4">
                            @if($posts->count())

                            @foreach($posts as $post)
                            <div class="col-md-6 col-lg-4">
                                <div class="card">
                                    <img class="img-fluid" src="{{ asset('/storage/' . $post->image) }}" alt="{{ $post->name }}">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $post->title }}</h5>
                                        <p class="card-text">{{ $post->description }}</p>
                                    </div>
                                    <div class="card-footer">
                                        <p class="card-text d-inline"><small class="text-muted">@if($post->updated_at) Last updated
                                            {{\Carbon\Carbon::parse($post->updated_at)->diffForHumans()}} @else Posted {{\Carbon\Carbon::parse($post->created_at)->diffForHumans()}} @endif</small>
                                        </p><a href="{{ $post->id }}" class="card-link float-right"><small>Read more</small></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                                    <div class="ml-3">
                                        <h2 class="text-center"> User has no posts!</h2>
                                    </div>
                            @endif
                        </div>
                    </div>
                </div>
            <div class="row">

            </div>
        </div>
    </div>
@endsection
