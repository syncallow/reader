@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Post</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.posts.update', $post->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label>Title <span class="text-danger">*</span></label>
                                        <input type="text" name="title" class="form-control" placeholder="Title" value="{{ $post->title ?? old('title') }}" required>
                                        @error('title')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Description <span class="text-danger">*</span></label>
                                        <textarea name="description" class="form-control h-150px" rows="6" id="description" required>{{ $post->description ?? old('description') }}</textarea>
                                        @error('description')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Content <span class="text-danger">*</span></label>
                                        <textarea class="summernote" id="summernote" name="content">{{ $post->content ?? old('content') }}</textarea>
                                        @error('content')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Main Image</label>
                                            @if($post->image)
                                                <div class="p-2">
                                                    <img class="img-fluid" src="{{ asset('/storage/' . $post->image) }}" alt="">
                                                </div>
                                            @endif
                                            <input type="file" class="form-control-file" name="image">
                                            @error('image')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group offset-md-1 col-md-8">
                                            <label>Additional Images (select images via checkbox which you want to delete)</label>
                                            @if($post->postImages->count())
                                                <div class="p-2 d-flex h-auto">
                                                    @foreach($post->postImages as $postImage)
                                                        <div class="col-4">
                                                            <img class="img-fluid" src="{{ asset('/storage/' . $postImage->file_path) }}" alt="">
                                                            <input class="form-check-input" type="checkbox" name="delete_images[]" value="{{ $postImage->id }}">
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <input type="file" class="form-control-file" name="post_images[]" multiple>
                                            @error('post_images[]')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label>Category <span class="text-danger">*</span></label>
                                            <select id="category_id" class="form-control" name="category_id" required>
                                                <option selected="selected">Choose...</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" {{ $post->category_id == $category->id ? 'selected' : ''}}>{{ $category->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Tags</label>
                                            <select name="tags[]" multiple="multiple" class="form-control" id="tags">
                                                <option>Choose...</option>
                                                @foreach($tags as $tag)
                                                    <option value="{{ $tag->id }}" {{ is_array($post->tags->pluck('id')->toArray()) && in_array($tag->id, $post->tags->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $tag->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('tags[]')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Author <span class="text-danger">*</span></label>
                                            <select id="author_id" class="form-control" name="author_id" required>
                                                <option selected="selected">Choose...</option>
                                                @foreach($users as $user)
                                                    <option value="{{ $user->id }}" {{ $post->author_id == $user->id ? 'selected' : ''}}>{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('author_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-dark">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
