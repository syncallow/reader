@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create Post</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.posts.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                        <div class="form-group">
                                            <label>Title <span class="text-danger">*</span></label>
                                            <input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title') }}" required>
                                            @error('title')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Description <span class="text-danger">*</span></label>
                                            <textarea name="description" class="form-control h-150px" rows="6" id="description" required>{{ old('description') }}</textarea>
                                            @error('description')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Content <span class="text-danger">*</span></label>
                                            <textarea class="summernote" id="summernote" name="content">{{ old('content') }}</textarea>
                                            @error('content')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                      <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Main Image</label>
                                            <input type="file" class="form-control-file" name="image">
                                            @error('image')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Additional Images</label>
                                            <input type="file" class="form-control-file" name="post_images[]" multiple>
                                            @error('post_images[]')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label>Category <span class="text-danger">*</span></label>
                                            <select id="category_id" class="form-control" name="category_id" required>
                                                <option selected="selected">Choose...</option>
                                                @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Tags</label>
                                            <select name="tags[]" multiple="multiple" class="form-control" id="tags">
                                                <option selected="selected">Choose...</option>
                                                @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}" {{ is_array(old('tags')) && in_array($tag->id, old('tags')) ? 'selected' : '' }}>{{ $tag->title }}</option>
                                                @endforeach
                                            </select>
                                            @error('tags[]')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Author <span class="text-danger">*</span></label>
                                            <select id="author_id" class="form-control" name="author_id" required>
                                                <option selected="selected">Choose...</option>
                                                @foreach($users as $user)
                                                    <option value="{{ $user->id }}" {{ old('author_id') == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('author_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-dark">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
