@extends('admin.layouts.main')

@section('content')
    <div class="content-body" style="min-height: 1092px">
        <div class="container-fluid">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Actions</h4>
                        @if(session('status'))
                            <div class="p-2">
                                <div class="alert alert-success alert-dismissible fade show">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button> {{ session('status') }}</div>
                            </div>
                        @endif
                            <a href="{{ route('admin.posts.create') }}" class="btn btn-outline-primary">Create a new Post</a>
                            <a href="{{ route('admin.posts.edit', $post->id) }}" class="btn btn-outline-success">Edit Post</a>
                            <a href="#" class="btn btn-outline-danger" onclick="event.preventDefault(); document.getElementById('delPost').submit();">Delete Post</a>
                            <form class="d-none" action="{{ route('admin.posts.delete', $post->id) }}" method="post" id="delPost">@csrf @method('DELETE')</form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="bootstrap-carousel">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    @if($post->postImages->count() > 0)
                                        @foreach($post->postImages as $postImg)
                                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration }}" class=""></li>
                                        @endforeach
                                    @endif
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="{{ asset('/storage/' . $post->image) }}" alt="First slide">
                                    </div>

                                    @foreach($post->postImages as $postImg)

                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="{{ asset('/storage/'. $postImg->file_path) }}" alt="Second slide">
                                        </div>
                                    @endforeach
                                </div><a class="carousel-control-prev" href="#carouselExampleIndicators" data-slide="prev"><span class="carousel-control-prev-icon"></span> <span class="sr-only">Previous</span> </a><a class="carousel-control-next" href="#carouselExampleIndicators" data-slide="next"><span class="carousel-control-next-icon"></span> <span class="sr-only">Next</span></a>

                            </div>
                        </div>
                        <div class="mt-4">
                            <h2>{{ $post->title }}</h2>
                            @if($post->tags->count() > 0)
                                @foreach($post->tags as $postTag)
                                    <span class="label label-pill label-secondary">{{ $postTag->title }}</span>
                                @endforeach
                            @endif
                        </div>

                        <div class="mt-4">
                            <h4>Description</h4>
                            <p>{{ $post->description }}</p>
                        </div>

                        <div class="mt-4">
                            <h4>Content</h4>
                            <p>{!! $post->content !!}</p>
                        </div>

                        <div class="mt-4">
                            <h4>Category</h4>
                            <p>{{ $post->category->title }}</p>
                        </div>

                        <div class="mt-4">
                            <h4>Author</h4>
                            <p>{{ $post->author->name }}</p>
                        </div>

                        <div class="mt-4">
                            <h4>Posted</h4>
                            <p>{{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
