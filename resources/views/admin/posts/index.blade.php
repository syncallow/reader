@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            @if(session('status'))
                <div class="p-2">
                    <div class="alert alert-success alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button> {{ session('status') }}</div>
                </div>
            @endif

            <div class="row">
                <div class="col-12 m-b-30">
                    <h4 class="d-inline">Posts</h4>
                    <p class="text-muted">There are all posts that was posted on this blog</p>

                    <div class="mb-3"><a href="{{ route('admin.posts.create') }}" class="btn mb-1 btn-outline-primary">Create Post</a></div>

                    <div class="row">
                        @foreach($posts as $post)
                        <div class="col-md-6 col-lg-3">
                            <div class="card">
                                <img class="img-fluid" src="{{ asset('/storage/' . $post->image) }}" alt="{{ $post->title }}">
                                <div class="card-body">
                                    <a href="{{ route('admin.posts.show', $post->id) }}"><h5 class="card-title">{{ $post->title }}</h5></a>
                                    <p class="card-text">{{ substr($post->description, 0, 91) }}...</p>
                                </div>
                                <div class="card-footer d-flex justify-content-between">
                                    <div>
                                        <small class="text-muted">Posted {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</small>
                                    </div>
                                    <div>
                                        <a href="{{ route('admin.posts.edit', $post->id) }}" class="card-link text-success"><small>Edit</small></a>
                                        <a href="#" class="card-link float-right text-danger" onclick="event.preventDefault(); document.getElementById('delPost').submit();"><small>Delete</small></a>
                                        <form class="d-none" action="{{ route('admin.posts.delete', $post->id) }}" method="post" id="delPost">@csrf @method('DELETE')</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- End Col -->
                    </div>
                    <div class="pt-2">
                        {{ $posts->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
