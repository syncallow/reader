@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if(session('status'))
                                <div class="p-2">
                                    <div class="alert alert-success alert-dismissible fade show">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                        </button> {{ session('status') }}</div>
                                </div>
                            @endif
                            <h4 class="card-title">Edit comment</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.comments.update', $comment->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <input class="form-control" type="text" readonly value="{{ $comment->author->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Message <span class="text-danger">*</span></label>
                                        <textarea name="message" class="form-control" rows="6" id="comment" required>{{ $comment->message }}</textarea>
                                        @error('message')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-dark">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
