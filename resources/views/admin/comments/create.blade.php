@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create a new comment</h4>
                            <p class="text-muted">Commment will be added with your name <strong>{{ auth()->user()->name }}</strong></p>
                            <div class="basic-form">
                                <form action="{{ route('admin.comments.store') }}" method="post">
                                    @csrf
                                    <div class="form-group col-md-6">
                                        <label>Select post (select one):</label>
                                        <select name="post_id" class="form-control" id="post_id" required>
                                            @foreach($posts as $post)
                                            <option value="{{ $post->id }}">{{ $post->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                        <div class="form-group col-md-6">
                                            <label>Message <span class="text-danger">*</span></label>
                                            <textarea name="message" class="form-control" rows="6" id="comment" required></textarea>
                                            @error('message')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    <button type="submit" class="btn btn-dark">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
