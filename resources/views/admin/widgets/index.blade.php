@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            @if(session('status'))
                <div class="p-2">
                    <div class="alert alert-success alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button> {{ session('status') }}</div>
                </div>
            @endif
                <div class="mb-3"><a href="{{ route('admin.widgets.create') }}" class="btn mb-1 btn-outline-primary">Create Widget</a></div>
            <div class="row">

                @foreach($widgets as $widget)
                <div class="col-md-6 col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $widget->title }}</h5>
                            <p class="card-text">{!! $widget->content !!}</p>
                            <div class="card-text d-flex justify-content-between">
                                <div>
                                    <small class="text-muted">Posted {{ \Carbon\Carbon::parse($widget->created_at)->diffForHumans() }}</small>
                                </div>
                                <div>
                                    <a href="{{ route('admin.widgets.edit', $widget->id) }}" class="card-link text-success"><small>Edit</small></a>
                                    <a href="#" class="card-link float-right text-danger" onclick="event.preventDefault(); document.getElementById('delWidget').submit();"><small>Delete</small></a>
                                    <form class="d-none" action="{{ route('admin.widgets.delete', $widget->id) }}" method="post" id="delWidget">@csrf @method('DELETE')</form>
                                </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
