@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create a new widget</h4>

                            <div class="basic-form">
                                <form action="{{ route('admin.widgets.store') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label>Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="title" placeholder="Title here" required>
                                    </div>
                                        <div class="form-group">
                                            <label>Content <span class="text-danger">*</span></label>
                                            <textarea class="summernote" id="summernote" name="content"></textarea>
                                            @error('content')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    <button type="submit" class="btn btn-dark">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
