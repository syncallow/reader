<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Dashboard</li>
            <li>
                <a href="{{ route('admin.index') }}" aria-expanded="false">
                    <i class="icon-speedometer menu-icon"></i><span class="nav-text">Dashboard</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin.pages.index') }}" aria-expanded="false">
                    <i class="icon-docs menu-icon"></i><span class="nav-text">Pages</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.tags.index') }}" aria-expanded="false">
                    <i class="icon-tag menu-icon"></i><span class="nav-text">Tags</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.categories.index') }}" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">Categories</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.roles.index') }}" aria-expanded="false">
                    <i class="icon-key menu-icon"></i><span class="nav-text">Roles</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.users.index') }}" aria-expanded="false">
                    <i class="icon-people menu-icon"></i><span class="nav-text">Users</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.posts.index') }}" aria-expanded="false">
                    <i class="icon-book-open menu-icon"></i><span class="nav-text">Posts</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.comments.index') }}" aria-expanded="false">
                    <i class="icon-bubbles menu-icon"></i><span class="nav-text">Comments</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.widgets.index') }}" aria-expanded="false">
                    <i class="icon-rocket menu-icon"></i><span class="nav-text">Widgets</span>
                </a>
            </li>
        </ul>
    </div>
</div>
