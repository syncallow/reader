@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Vertical Form</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.pages.store') }}" method="post">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Name <span class="text-danger">*</span></label>
                                            <input name="name" type="text" class="form-control" placeholder="Name" required>
                                            @error('name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Slug <span class="text-danger">*</span></label>
                                            <input name="slug" type="text" class="form-control" placeholder="Slug" required>
                                            @error('slug')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Title <span class="text-danger">*</span></label>
                                        <input name="title" type="text" class="form-control" placeholder="Page title" required>
                                        @error('title')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Content <span class="text-danger">*</span></label>
                                        <textarea class="summernote" id="summernote" name="content" class="form-control h-200px" rows="6" id="content"></textarea>
                                        @error('content')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Title</label>
                                        <input name="meta_title" type="text" class="form-control" placeholder="Page meta title">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <textarea name="meta_desc" class="form-control h-150px" rows="6" id="meta_desc"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Keywords</label>
                                        <textarea name="meta_keywords" class="form-control h-150px" rows="6" id="meta_keywords"></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-dark">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
