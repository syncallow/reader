@extends('admin.layouts.main')

@section('content')
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if(session('status'))
                                <div class="p-2">
                                    <div class="alert alert-success alert-dismissible fade show">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                        </button> {{ session('status') }}</div>
                                </div>
                            @endif
                            <h4 class="card-title">Edit {{ $page->name }} page</h4>
                            <div class="basic-form">
                                <form action="{{ route('admin.pages.update', $page->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Name <span class="text-danger">*</span></label>
                                            <input name="name" type="text" class="form-control" placeholder="Name" value="{{ $page->name }}" required>
                                            @error('name')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Slug <span class="text-danger">*</span></label>
                                            <input name="slug" type="text" class="form-control" placeholder="Slug" value="{{ $page->slug }}" required>
                                            @error('slug')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Title <span class="text-danger">*</span></label>
                                        <input name="title" type="text" class="form-control" placeholder="Page title" value="{{ $page->title }}" required>
                                        @error('title')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Content <span class="text-danger">*</span></label>
                                        <textarea class="summernote" id="summernote" name="content" class="form-control h-200px" rows="6" id="content">{{ $page->content }}</textarea>
                                        @error('content')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Title</label>
                                        <input name="meta_title" type="text" class="form-control" placeholder="Page meta title" value="{{ $page->meta_title }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <textarea name="meta_desc" class="form-control h-150px" rows="6" id="meta_desc">{{ $page->meta_desc }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Keywords</label>
                                        <textarea name="meta_keywords" class="form-control h-150px" rows="6" id="meta_keywords">{{ $page->meta_keywords }}</textarea>
                                    </div>

                                    <input type="hidden" name="page_id" value="{{ $page->id }}">

                                    <button type="submit" class="btn btn-dark">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #/ container -->
    </div>
@endsection
