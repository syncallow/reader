<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- mobile responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="This is meta description">

        <!-- plugins -->
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/themify-icons/themify-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" media="screen">

        <!--Favicon-->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @routes
        @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
        @inertiaHead
    </head>
    <body class="font-sans antialiased">
        @inertia
    </body>
    <!-- JS Plugins -->
    <script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>

    <script src="{{ asset('plugins/bootstrap/bootstrap.min.js') }}"></script>

    <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>

    <script src="{{ asset('plugins/instafeed/instafeed.min.js') }}"></script>


    <!-- Main Script -->
    <script src="{{ asset('js/script.js') }}"></script>
</html>
