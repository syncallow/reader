<?php


namespace App\Http\Filters;


use Illuminate\Database\Eloquent\Builder;

class PostFilter extends AbstractFilter
{

    const CATEGORY = 'category';
    const TAG = 'tag';
    const SEARCH = 'search';


    protected function getCallbacks(): array
    {
        return [
            self::CATEGORY => [$this, 'category'],
            self::TAG => [$this, 'tag'],
            self::SEARCH => [$this, 'search'],
        ];
    }

    protected function search(Builder $builder, $value) {
        $builder->where('title', 'like', "%{$value}%");
    }

    protected function category(Builder $builder, $value) {
        $builder->where('category_id', $value);
    }

    protected function tag(Builder $builder, $value) {
        $builder->whereHas('tags', function ($b) use ($value){
            $b->where('tag_id', $value);
        });
    }
}
