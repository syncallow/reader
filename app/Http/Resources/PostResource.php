<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
       return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->imageUrl,
            'author_name' => $this->author->name,
            'author_id' => $this->author_id,
            'content' => $this->content,
            'category_id' => $this->category_id,
            'author_img' => $this->author->profileImage,
            'date' => $this->created_at->format('d M, Y'),
            'tags' => TagResource::collection($this->tags),
            'post_images' => PostImageResource::collection($this->postImages)
       ];
    }
}
