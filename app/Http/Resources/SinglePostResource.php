<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SinglePostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'image' => $this->imageUrl,
            'author_name' => $this->author->name,
            'author_img' => $this->author->profileImage,
            'author_id' => $this->author_id,
            'date' => $this->created_at->format('d M, Y'),
            'tags' => TagResource::collection($this->tags),
            'post_images' => PostImageResource::collection($this->postImages),
            'comments' => CommentResource::collection($this->comments)
        ];
    }
}
