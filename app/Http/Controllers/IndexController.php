<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoriesIndexResource;
use App\Http\Resources\PostResource;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Models\Widget;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index() {
        $tags = Tag::all();
        $trendingPosts = PostResource::collection(Post::latest()->take(3)->get());
        $recentPosts = PostResource::collection(Post::latest()->take(3)->get());
        $posts = PostResource::collection(Post::paginate(3));
        $authors = AuthorResource::collection(User::take(3)->get())->resolve();
        $categoriesIndex = CategoriesIndexResource::collection(Category::all());
        $widgets = Widget::first();
        return inertia('Index', compact('tags', 'posts', 'trendingPosts', 'authors', 'categoriesIndex', 'widgets', 'recentPosts'));
    }
}
