<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoriesIndexResource;
use App\Http\Resources\PostResource;
use App\Models\Category;
use App\Models\Page;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Models\Widget;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index() {
        $page = Page::whereId(3)->first();
        return inertia('About', compact('page'));
    }
}
