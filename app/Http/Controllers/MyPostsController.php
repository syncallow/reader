<?php

namespace App\Http\Controllers;

use App\Http\Filters\PostFilter;
use App\Http\Requests\Post\ContentImage\DeleteRequest;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoriesIndexResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\SinglePostResource;
use App\Models\Category;
use App\Models\Post;
use App\Models\PostImage;
use App\Models\Tag;
use App\Models\User;
use App\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Spatie\Searchable\Search;

class MyPostsController extends Controller
{
    public function index() {
        //$this->authorize('postManage', Post::class);
        $posts = PostResource::collection(auth()->user()->posts)->paginate(2);

        return inertia('Profile/MyPosts', compact('posts'));
    }

    public function create(){
        $categories = Category::all();
        $tags = Tag::all();

        return inertia('Profile/PostAdd', compact('categories', 'tags'));
    }

    public function storeContentImage(\App\Http\Requests\Post\ContentImage\StoreRequest $request){
        $data = $request->validated();
        $image = $data['image'];
        $name = md5(Carbon::now() . '_' . $image->getClientOriginalName()) . '.' .$image->getClientOriginalExtension();
        $filePath = Storage::disk('public')->putFileAs('/images', $image, $name);

        return response()->json(['url' => url('/storage/' . $filePath)]);
    }

    public function deleteContentImage(DeleteRequest $request){
        $data = $request->validated();
        $removeStr = $request->root() . '/storage/';
        $path = str_replace($removeStr, '', $data['url']);
        Storage::disk('public')->delete($path);

        return response()->json(['message' => 'success']);
    }

    public function store(StoreRequest $request) {
        $data = $request->validated();

        if (isset($data['images'])) {
            $postImages = $data['images'];
            unset($data['images']);
        }
        if (isset($data['image'])) {
            $data['image'] = Storage::disk('public')->put('/images', $data['image']);
        }
        if (isset($data['tags'])) {
            $tagsIds = $data['tags'];
            unset($data['tags']);
        }

        $post = Post::firstOrCreate($data);

        if (isset($tagsIds)){
            $post->tags()->attach($tagsIds);
        }
        if(isset($postImages)) {
            foreach ($postImages as $postImage){
                $filePath = Storage::disk('public')->put('/images', $postImage);
                PostImage::create([
                    'post_id' => $post->id,
                    'file_path' => $filePath
                ]);
            }
        }

        return to_route('my.posts.index')->with('message', 'Post added successfully!');
    }

    public function edit(Post $post){
        $this->authorize('postManage', $post);
        $categories = Category::all();
        $tags = Tag::all();
        $post = new PostResource($post);

        return inertia('Profile/PostEdit', compact('post', 'tags', 'categories'));
    }

    public function update(UpdateRequest $request, Post $post) {
        $data = $request->validated();

        if (!$data['image']){
            unset($data['image']);
        }
        if (isset($data['images']) && !$data['images']){
            unset($data['images']);
        }

        $postImages = isset($data['images']) ? $data['images'] : null;
        unset($data['images']);

        if (isset($data['deleteImages'])) {
            $currentImages = $post->postImages;

            foreach ($currentImages as $currentImage){
                if (in_array($currentImage->id, $data['deleteImages'])){
                    Storage::disk('public')->delete($currentImage->file_path);
                    $currentImage->delete();
                }
            }
            unset($data['deleteImages']);
        }

        if (isset($data['image'])){
            $data['image'] = Storage::disk('public')->put('/images',$data['image']);
        }

        $tags = isset($data['tags']) ? $data['tags'] : null;
        unset($data['tags']);

        $post->update($data);

        if (isset($tags)){
            $tagsIds = [];
            foreach ($tags as $tag) {
               array_push($tagsIds, $tag['id']);
            }
            $post->tags()->sync($tagsIds);
        }

        if ($postImages){
            foreach ($postImages as $postImage){
                $filePath = Storage::disk('public')->put('/images', $postImage);
                PostImage::updateOrCreate([
                   'post_id' => $post->id,
                    'file_path' => $filePath
                ]);
            }
        }

        return to_route('my.posts.edit', $post->id)->with('message', 'Post Updated Successfully!');
    }

    public function delete(Post $post){
        $additionalImages = $post->postImages;
        if ($additionalImages){
            foreach ($additionalImages as $additionalImage){
                Storage::disk('public')->delete($additionalImage->file_path);
            }
        }

        Storage::disk('public')->delete($post->image);

        $post->delete();

        return to_route('my.posts.index')->with('message', 'Post was deleted successfully!');

    }

}
