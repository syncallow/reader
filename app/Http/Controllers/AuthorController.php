<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoriesIndexResource;
use App\Http\Resources\PostResource;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Models\Widget;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(User $author) {

        $posts = PostResource::collection($author->posts)->paginate(2);


        return inertia('Author', compact('author', 'posts'));
    }
}
