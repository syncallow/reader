<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\StoreRequest;
use App\Models\Comment;
use App\Models\Post;

class CommentController extends Controller
{

    public function store(StoreRequest $request){
        $data = $request->validated();
        Comment::create($data);
        return redirect()->back()->with('message','Comment was added successfully!');
    }
}
