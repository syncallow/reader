<?php

namespace App\Http\Controllers;

use App\Http\Filters\PostFilter;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoriesIndexResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\SinglePostResource;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Models\Widget;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class PostController extends Controller
{
    public function index() {
        //$this->authorize('postManage', Post::class);
        $tags = Tag::all();
        $trendingPosts = PostResource::collection(Post::latest()->take(3)->get());
        $recentPosts = PostResource::collection(Post::latest()->take(3)->get());
        $posts = PostResource::collection(Post::paginate(3));
        $authors = AuthorResource::collection(User::take(3)->get())->resolve();
        $categoriesIndex = CategoriesIndexResource::collection(Category::all());
        $widgets = Widget::first();
        return inertia('Index', compact('tags', 'posts', 'trendingPosts', 'authors', 'categoriesIndex', 'widgets', 'recentPosts'));
    }

    public function show(Post $post){
        $post = new SinglePostResource($post);

        return inertia('Post', compact('post'));
    }

    public function postsByCategory(Category $category) {
        $categoryFilter = [
            'category' => $category->id,
        ];
        $allTags = Tag::all();
        $allCategories = CategoriesIndexResource::collection(Category::all());
        $authors = AuthorResource::collection(User::take(3)->get())->resolve();
        $widgets = Widget::first();
        $recentPosts = PostResource::collection(Post::latest()->take(3)->get());
        $filter = app()->make(PostFilter::class, ['queryParams' => array_filter($categoryFilter)]);
        $posts = Post::filter($filter)->paginate(3);
        $posts = PostResource::collection($posts);

        return inertia('PostsByCategory', compact('posts', 'category','allTags','allCategories','authors','widgets','recentPosts'));
    }

    public function postsByTag(Tag $tag) {
        $tagFilter = [
            'tag' => $tag->id,
        ];
        $allTags = Tag::all();
        $categories = CategoriesIndexResource::collection(Category::all());
        $authors = AuthorResource::collection(User::take(3)->get())->resolve();
        $widgets = Widget::first();
        $recentPosts = PostResource::collection(Post::latest()->take(3)->get());
        $filter = app()->make(PostFilter::class, ['queryParams' => array_filter($tagFilter)]);
        $posts = Post::filter($filter)->paginate(3);
        $posts = PostResource::collection($posts);

        return inertia('PostsByTag', compact('posts', 'tag','allTags','categories','authors','widgets','recentPosts'));
    }

    public function search(Request $request) {
        //$request = $request->input('params');
        $request = $request->query('search');

        //$filter = app()->make(PostFilter::class, ['queryParams' => array_filter($data)]);

        //$posts = PostResource::collection(Post::filter($filter));

        if (!$request){
            return  response()->json(null);
        }
        $data = (new Search())
            ->registerModel(Post::class, 'title')
            ->search($request);
        return response()->json($data);
    }
}
