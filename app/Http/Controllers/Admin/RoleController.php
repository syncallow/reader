<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreRequest;
use App\Http\Requests\Admin\Tag\UpdateRequest;
use App\Models\Role;

class RoleController extends Controller
{
    public function index() {
        $roles = Role::paginate(7);
        return view('admin.roles.index',compact('roles'));
    }

    public function create() {
        return view('admin.roles.create');
    }

    public function store(StoreRequest $request){
        $data = $request->validated();

        Role::create($data);

        return redirect()->route('admin.roles.index')->with('status', 'Role successfully created');
    }

    public function delete(Role $role) {
        $role->delete();

        return redirect()->route('admin.roles.index')->with('status', 'Role deleted successfully');
    }

    public function edit(Role $role) {
        return view('admin.roles.edit', compact('role'));
    }

    public function update(UpdateRequest $request, Role $role){
        $data = $request->validated();
        $role->update($data);

        return redirect()->route('admin.roles.edit',compact('role'))->with('status', 'Role updated successfully!');
    }
}
