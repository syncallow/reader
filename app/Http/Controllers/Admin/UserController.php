<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreRequest;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(){
        $users = User::paginate(5);
        return view('admin.users.index', compact('users'));
    }

    public function create() {
        $roles = Role::all();
        return view('admin.users.create', compact('roles'));
    }

    public function store(StoreRequest $request) {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);
        if (isset($data['profile_image'])){
            $data['profile_image'] = Storage::disk('public')->put('/images', $data['profile_image']);
        }
        User::firstOrCreate($data);
        return redirect()->route('admin.users.index')->with('status', "User created successfully.");
    }

    public function edit(User $user) {
        $roles = Role::all();
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(UpdateRequest $request, User $user){
        $data = $request->validated();
        if (!$data['password']) {
            unset($data['password']);
        }else{
            $data['password'] = Hash::make($data['password']);
        }
        if (isset($data['profile_image'])){
            $data['profile_image'] = Storage::disk('public')->put('/images', $data['profile_image']);
        }

        $user->update($data);

        return redirect()->route('admin.users.index')->with('status', 'User updated successfully.');
    }

    public function show(User $user){
        return  view('admin.users.show', compact('user'));
    }

    public function delete(User $user){
        $user->delete();
        return redirect()->route('admin.users.index')->with('status', 'User deleted successfully.');
    }
}
