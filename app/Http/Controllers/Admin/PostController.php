<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\StoreRequest;
use App\Http\Requests\Admin\Post\UpdateRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\PostImage;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function index(){
        $posts = Post::paginate(4);
        return view('admin.posts.index', compact('posts'));
    }

    public function create() {
        $categories = Category::all();
        $tags = Tag::all();
        $users = User::all();
        return view('admin.posts.create', compact('categories', 'tags', 'users'));
    }

    public function store(StoreRequest $request) {
        $data = $request->validated();

            if (isset($data['post_images'])){
                $postImages = $data['post_images'];
                unset($data['post_images']);
            }
        if (isset($data['image'])){
            $data['image'] = Storage::disk('public')->put('/images', $data['image']);
        }

        if (isset($data['tags'])){
            $tagsIds = $data['tags'];
            unset($data['tags']);
        }

        $post = Post::firstOrCreate($data);

        if (isset($tagsIds)){
            $post->tags()->attach($tagsIds);
        }

        if (isset($postImages)) {
            foreach ($postImages as $postImage){
                $filePath = Storage::disk('public')->put('/images', $postImage);
                PostImage::create([
                    'post_id' => $post->id,
                    'file_path' => $filePath
                ]);
            }
        }

        return redirect()->route('admin.posts.index')->with('status', "Post created successfully.");
    }

    public function edit(Post $post) {
        $categories = Category::all();
        $tags = Tag::all();
        $users = User::all();
        return view('admin.posts.edit', compact('post', 'categories','tags', 'users'));
    }

    public function update(UpdateRequest $request, Post $post){
        $data = $request->validated();

        if (isset($data['delete_images'])){
            $post->postImages()->whereIn('id', $data['delete_images'])->delete();
            unset($data['delete_images']);
        }

        if (isset($data['post_images'])){
            $postImgs = $data['post_images'];
            unset($data['post_images']);
        }

        if (isset($data['image'])){
            $data['image'] = Storage::disk('public')->put('/images', $data['image']);
        }

        if (isset($data['tags'])){
            $tagsIds = $data['tags'];
            unset($data['tags']);
        }

        $post->update($data);

        if (isset($tagsIds)){
            $post->tags()->sync($tagsIds);
        }

        if (isset($postImgs)) {
            foreach ($postImgs as $postImage){
                $filePath = Storage::disk('public')->put('/images', $postImage);
                PostImage::updateOrCreate([
                    'post_id' => $post->id,
                    'file_path' => $filePath
                ]);
            }
        }

        return redirect()->route('admin.posts.show', compact('post'))->with('status', 'Post updated successfully.');
    }

    public function show(Post $post){
        return  view('admin.posts.show', compact('post'));
    }

    public function delete(Post $post){
        $post->delete();
        return redirect()->route('admin.posts.index')->with('status', 'Post deleted successfully.');
    }
}
