<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreRequest;
use App\Http\Requests\Admin\Tag\UpdateRequest;
use App\Models\Tag;

class TagController extends Controller
{
    public function index() {
        $tags = Tag::paginate(7);
        return view('admin.tags.index',compact('tags'));
    }

    public function create() {
        return view('admin.tags.create');
    }

    public function store(StoreRequest $request){
        $data = $request->validated();

        Tag::create($data);

        return redirect()->route('admin.tags.index')->with('status', 'Tag successfully created');
    }

    public function delete(Tag $tag) {
        $tag->delete();

        return redirect()->route('admin.tags.index')->with('status', 'Tag deleted successfully');
    }

    public function edit(Tag $tag) {
        return view('admin.tags.edit', compact('tag'));
    }

    public function update(UpdateRequest $request, Tag $tag){
        $data = $request->validated();
        $tag->update($data);

        return redirect()->route('admin.tags.edit',compact('tag'))->with('status', 'Tag updated successfully!');
    }
}
