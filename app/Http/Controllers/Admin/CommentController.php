<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Comment\StoreRequest;
use App\Http\Requests\Admin\Comment\UpdateRequest;
use App\Models\Comment;
use App\Models\Post;

class CommentController extends Controller
{
    public function index() {
        $comments = Comment::paginate(7);
        return view('admin.comments.index',compact('comments'));
    }

    public function create() {
        $posts = Post::all();
        return view('admin.comments.create', compact('posts'));
    }

    public function store(StoreRequest $request){
        $data = $request->validated();

        $data['author_id'] = auth()->user()->id;
        Comment::create($data);

        return redirect()->route('admin.comments.index')->with('status', 'Comment successfully created');
    }

    public function delete(Comment $comment) {
        $comment->delete();

        return redirect()->route('admin.comments.index')->with('status', 'Comment deleted successfully');
    }

    public function edit(Comment $comment) {
        return view('admin.comments.edit', compact('comment'));
    }

    public function update(UpdateRequest $request, Comment $comment){
        $data = $request->validated();
        $comment->update($data);

        return redirect()->route('admin.comments.edit',compact('comment'))->with('status', 'Comment updated successfully!');
    }
}
