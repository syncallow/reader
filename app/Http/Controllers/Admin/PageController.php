<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Page\StoreRequest;
use App\Http\Requests\Admin\Page\UpdateRequest;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index() {
        $pages = Page::all();
        return view('admin.pages.index',compact('pages'));
    }

    public function create() {
        return view('admin.pages.create');
    }

    public function store(StoreRequest $request){
        $data = $request->validated();

        Page::create($data);

        return redirect()->route('admin.pages.index')->with('status', 'Page successfully created');
    }

    public function delete(Page $page) {
        $page->delete();

        return redirect()->route('admin.pages.index')->with('status', 'Page deleted successfully');
    }

    public function edit(Page $page) {
        return view('admin.pages.edit', compact('page'));
    }

    public function update(UpdateRequest $request, Page $page){
        $data = $request->validated();
        unset($data['page_id']);
        $page->update($data);

        return redirect()->route('admin.pages.edit',compact('page'))->with('status', 'Page updated successfully!');
    }
}
