<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Widget\StoreRequest;
use App\Http\Requests\Admin\Widget\UpdateRequest;
use App\Models\Widget;
use Illuminate\Http\Request;

class WidgetController extends Controller
{
    public function index() {
        $widgets = Widget::all();
        return view('admin.widgets.index',compact('widgets'));
    }

    public function create() {
        return view('admin.widgets.create');
    }

    public function store(StoreRequest $request){
        $data = $request->validated();

        Widget::firstOrCreate($data);

        return redirect()->route('admin.widgets.index')->with('status', 'Widget successfully created');
    }

    public function delete(Widget $widget) {
        $widget->delete();

        return redirect()->route('admin.widgets.index')->with('status', 'Widget deleted successfully');
    }

    public function edit(Widget $widget) {
        return view('admin.widgets.edit', compact('widget'));
    }

    public function update(UpdateRequest $request, Widget $widget){
        $data = $request->validated();

        $widget->update($data);

        return redirect()->route('admin.widgets.edit',compact('widget'))->with('status', 'Widget updated successfully!');
    }
}
