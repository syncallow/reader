<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreRequest;
use App\Http\Requests\Admin\Tag\UpdateRequest;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index() {
        $categories = Category::paginate(7);
        return view('admin.categories.index',compact('categories'));
    }

    public function create() {
        return view('admin.categories.create');
    }

    public function store(StoreRequest $request){
        $data = $request->validated();

        Category::create($data);

        return redirect()->route('admin.categories.index')->with('status', 'Category successfully created');
    }

    public function delete(Category $category) {
        $category->delete();

        return redirect()->route('admin.categories.index')->with('status', 'Category deleted successfully');
    }

    public function edit(Category $category) {
        return view('admin.categories.edit', compact('category'));
    }

    public function update(UpdateRequest $request, Category $category){
        $data = $request->validated();
        $category->update($data);

        return redirect()->route('admin.categories.edit',compact('category'))->with('status', 'Category updated successfully!');
    }
}
