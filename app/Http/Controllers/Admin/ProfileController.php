<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(User $user)
    {
        $posts = Post::latest()->take(3)->get();

        return view('admin.profile', compact('user', 'posts'));
    }
}
