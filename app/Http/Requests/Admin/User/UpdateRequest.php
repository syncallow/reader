<?php

namespace App\Http\Requests\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'password' => 'nullable|string|confirmed',
            'job' => 'nullable|string',
            'about_me' => 'nullable|string',
            'profile_image' => 'nullable|file',
            'facebook_link' => 'nullable|string',
            'twitter_link' => 'nullable|string',
            'github_link' => 'nullable|string',
            'web_link' => 'nullable|string',
            'role_id' => 'required|integer|exists:roles,id',
        ];
    }
}
