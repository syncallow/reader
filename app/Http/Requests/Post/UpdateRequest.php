<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'image' => 'nullable|file',
            'images' => 'nullable|array',
            'deleteImages' => 'nullable|array',
            'description' => 'required|string',
            'content' => 'nullable',
            'tags' => 'nullable|array',
            'category_id' => 'required',
        ];
    }
}
