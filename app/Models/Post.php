<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Post extends Model implements Searchable
{
    use HasFactory, Filterable;

    protected $guarded = false;
    protected $table = 'posts';

    public function getSearchResult(): SearchResult
    {
        $url = $this->id;
        return  new SearchResult(
            $this,
            $this->title,
            $url
        );
    }

    public function author(){
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }

    public function postImages(){
        return $this->hasMany(PostImage::class, 'post_id', 'id');
    }

    public function getImageUrlAttribute(){
        return url('storage/' . $this->image);
    }

}
