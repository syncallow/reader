<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function (){
    Route::get('/', \App\Http\Controllers\Admin\IndexController::class)->name('admin.index');
    Route::get('/profile/{user}',[\App\Http\Controllers\Admin\ProfileController::class, 'index'])->name('admin.profile');
    Route::group(['prefix' => 'pages'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\PageController::class, 'index'])->name('admin.pages.index');
        Route::get('/create', [\App\Http\Controllers\Admin\PageController::class, 'create'])->name('admin.pages.create');
        Route::post('/', [\App\Http\Controllers\Admin\PageController::class, 'store'])->name('admin.pages.store');
        Route::get('/edit/{page}', [\App\Http\Controllers\Admin\PageController::class, 'edit'])->name('admin.pages.edit');
        Route::patch('/update/{page}', [\App\Http\Controllers\Admin\PageController::class, 'update'])->name('admin.pages.update');
        Route::delete('/{page}', [\App\Http\Controllers\Admin\PageController::class, 'delete'])->name('admin.pages.delete');
    });

    Route::group(['prefix' => 'tags'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\TagController::class, 'index'])->name('admin.tags.index');
        Route::get('/create', [\App\Http\Controllers\Admin\TagController::class, 'create'])->name('admin.tags.create');
        Route::post('/', [\App\Http\Controllers\Admin\TagController::class, 'store'])->name('admin.tags.store');
        Route::get('/edit/{tag}', [\App\Http\Controllers\Admin\TagController::class, 'edit'])->name('admin.tags.edit');
        Route::patch('/update/{tag}', [\App\Http\Controllers\Admin\TagController::class, 'update'])->name('admin.tags.update');
        Route::delete('/{tag}', [\App\Http\Controllers\Admin\TagController::class, 'delete'])->name('admin.tags.delete');
    });

    Route::group(['prefix' => 'categories'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('admin.categories.index');
        Route::get('/create', [\App\Http\Controllers\Admin\CategoryController::class, 'create'])->name('admin.categories.create');
        Route::post('/', [\App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('admin.categories.store');
        Route::get('/edit/{category}', [\App\Http\Controllers\Admin\CategoryController::class, 'edit'])->name('admin.categories.edit');
        Route::patch('/update/{category}', [\App\Http\Controllers\Admin\CategoryController::class, 'update'])->name('admin.categories.update');
        Route::delete('/{category}', [\App\Http\Controllers\Admin\CategoryController::class, 'delete'])->name('admin.categories.delete');
    });

    Route::group(['prefix' => 'roles'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\RoleController::class, 'index'])->name('admin.roles.index');
        Route::get('/create', [\App\Http\Controllers\Admin\RoleController::class, 'create'])->name('admin.roles.create');
        Route::post('/', [\App\Http\Controllers\Admin\RoleController::class, 'store'])->name('admin.roles.store');
        Route::get('/edit/{role}', [\App\Http\Controllers\Admin\RoleController::class, 'edit'])->name('admin.roles.edit');
        Route::patch('/update/{role}', [\App\Http\Controllers\Admin\RoleController::class, 'update'])->name('admin.roles.update');
        Route::delete('/{role}', [\App\Http\Controllers\Admin\RoleController::class, 'delete'])->name('admin.roles.delete');
    });

    Route::group(['prefix' => 'users'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.users.index');
        Route::get('/create', [\App\Http\Controllers\Admin\UserController::class, 'create'])->name('admin.users.create');
        Route::post('/', [\App\Http\Controllers\Admin\UserController::class, 'store'])->name('admin.users.store');
        Route::get('/show/{user}', [\App\Http\Controllers\Admin\UserController::class, 'show'])->name('admin.users.show');
        Route::get('/edit/{user}', [\App\Http\Controllers\Admin\UserController::class, 'edit'])->name('admin.users.edit');
        Route::patch('/update/{user}', [\App\Http\Controllers\Admin\UserController::class, 'update'])->name('admin.users.update');
        Route::delete('/{user}', [\App\Http\Controllers\Admin\UserController::class, 'delete'])->name('admin.users.delete');
    });

    Route::group(['prefix' => 'posts'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\PostController::class, 'index'])->name('admin.posts.index');
        Route::get('/create', [\App\Http\Controllers\Admin\PostController::class, 'create'])->name('admin.posts.create');
        Route::post('/', [\App\Http\Controllers\Admin\PostController::class, 'store'])->name('admin.posts.store');
        Route::get('/show/{post}', [\App\Http\Controllers\Admin\PostController::class, 'show'])->name('admin.posts.show');
        Route::get('/edit/{post}', [\App\Http\Controllers\Admin\PostController::class, 'edit'])->name('admin.posts.edit');
        Route::patch('/update/{post}', [\App\Http\Controllers\Admin\PostController::class, 'update'])->name('admin.posts.update');
        Route::delete('/{post}', [\App\Http\Controllers\Admin\PostController::class, 'delete'])->name('admin.posts.delete');
    });

    Route::group(['prefix' => 'comments'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\CommentController::class, 'index'])->name('admin.comments.index');
        Route::get('/create', [\App\Http\Controllers\Admin\CommentController::class, 'create'])->name('admin.comments.create');
        Route::post('/', [\App\Http\Controllers\Admin\CommentController::class, 'store'])->name('admin.comments.store');
        Route::get('/edit/{comment}', [\App\Http\Controllers\Admin\CommentController::class, 'edit'])->name('admin.comments.edit');
        Route::patch('/update/{comment}', [\App\Http\Controllers\Admin\CommentController::class, 'update'])->name('admin.comments.update');
        Route::delete('/{comment}', [\App\Http\Controllers\Admin\CommentController::class, 'delete'])->name('admin.comments.delete');
    });
    Route::group(['prefix' => 'widgets'], function (){
        Route::get('/', [\App\Http\Controllers\Admin\WidgetController::class, 'index'])->name('admin.widgets.index');
        Route::get('/create', [\App\Http\Controllers\Admin\WidgetController::class, 'create'])->name('admin.widgets.create');
        Route::post('/', [\App\Http\Controllers\Admin\WidgetController::class, 'store'])->name('admin.widgets.store');
        Route::get('/edit/{widget}', [\App\Http\Controllers\Admin\WidgetController::class, 'edit'])->name('admin.widgets.edit');
        Route::patch('/update/{widget}', [\App\Http\Controllers\Admin\WidgetController::class, 'update'])->name('admin.widgets.update');
        Route::delete('/{widget}', [\App\Http\Controllers\Admin\WidgetController::class, 'delete'])->name('admin.widgets.delete');
    });
});

Route::get('/', [\App\Http\Controllers\IndexController::class, 'index'])->name('index');
Route::get('/about', [\App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::get('/contact', [\App\Http\Controllers\ContactController::class, 'index'])->name('contact');
Route::get('/author/{author}', [\App\Http\Controllers\AuthorController::class, 'index'])->name('author');

Route::group(['prefix' => 'posts'], function(){
    Route::get('/{post}',[\App\Http\Controllers\PostController::class, 'show'])->name('posts.show');
    Route::get('/tag/{tag}', [\App\Http\Controllers\PostController::class, 'postsByTag'])->name('posts.by.tag');
    Route::get('/category/{category}', [\App\Http\Controllers\PostController::class, 'postsByCategory'])->name('posts.by.category');
    Route::post('/add-comment', [\App\Http\Controllers\CommentController::class, 'store'])->name('posts.comment.store');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::group(['prefix' => 'my-posts'], function (){
       Route::get('/', [\App\Http\Controllers\MyPostsController::class, 'index'])->name('my.posts.index');
       Route::get('/create', [\App\Http\Controllers\MyPostsController::class, 'create'])->name('my.posts.create');
       Route::post('/', [\App\Http\Controllers\MyPostsController::class, 'store'])->name('my.posts.store');
       Route::get('/edit/{post}', [\App\Http\Controllers\MyPostsController::class, 'edit'])->name('my.posts.edit');
       Route::patch('/update/{post}', [\App\Http\Controllers\MyPostsController::class, 'update'])->name('my.posts.update');
       Route::delete('/delete/{post}', [\App\Http\Controllers\MyPostsController::class, 'delete'])->name('my.posts.delete');

       Route::group(['prefix' => 'images'] ,function (){
           Route::post('/', [\App\Http\Controllers\MyPostsController::class, 'storeContentImage'])->name('store.content.image');
           Route::post('/delete', [\App\Http\Controllers\MyPostsController::class, 'deleteContentImage'])->name('delete.content.image');
       });
    });
});

require __DIR__.'/auth.php';
